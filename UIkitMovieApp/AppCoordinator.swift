//
//  AppCoordinator.swift
//  UIkitMovieApp
//
//  Created by Szabó Ágoston on 2020. 04. 09..
//  Copyright © 2020. Szabo Agoston. All rights reserved.
//

//import UIKit
//
//class AppCoordinator {
//    
//    private let window: UIWindow
//    
//    init(window: UIWindow) {
//        self.window = window
//    }
//    
//    func start() {
//        let viewController = RootViewController()
//        window.rootViewController = viewController
//        window.makeKeyAndVisible()
//    }
//    
//}
