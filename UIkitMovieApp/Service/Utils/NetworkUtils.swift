//
//  NetworkUtils.swift
//  UIkitMovieApp
//
//  Created by Szabó Ágoston on 2020. 04. 09..
//  Copyright © 2020. Szabo Agoston. All rights reserved.
//

import SystemConfiguration

public class NetworkUtils {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        /* Only Working for WIFI
         let isReachable = flags == .reachable
         let needsConnection = flags == .connectionRequired
         
         return isReachable && !needsConnection
         */
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
        
    }
    
    
    enum ERROR_CODE: Int {
        case ERROR_VALIDATION_FAILED = 4001
        case ERROR_EMAIL_ALREADY_REGISTERED = 4002
        case ERROR_AUTHENTICATION_FAILED = 4003
        case ERROR_PASSWORD_NOT_SET = 4004
        case ERROR_UNSUPPORTED_FILE_FORMAT = 4005
        case ERROR_EMAIL_NOT_REGISTERED = 4006
        case ERROR_NO_INTERNET = -1
        case ERROR_INTERNAL_SERVER_ERROR = -2
        case ERROR_REQUEST_CANCELED = -3
        case ERROR_CLIENT_SIDE = -4
        case AUTHENTICATION_FAILED_SHOULD_LOGOUT = -5
    }
    
    struct ERROR_CODE_INT {
        static let ERROR_VALIDATION_FAILED = 4001
        static let ERROR_EMAIL_ALREADY_REGISTERED = 4002
        static let ERROR_AUTHENTICATION_FAILED = 4003
        static let ERROR_PASSWORD_NOT_SET = 4004
        static let ERROR_UNSUPPORTED_FILE_FORMAT = 4005
        static let ERROR_EMAIL_NOT_REGISTERED = 4006
        static let ERROR_NO_INTERNET = -1
        static let ERROR_INTERNAL_SERVER_ERROR = -2
        static let ERROR_REQUEST_CANCELED = -3
        static let ERROR_CLIENT_SIDE = -4
        static let AUTHENTICATION_FAILED_SHOULD_LOGOUT = -5
    }
}



