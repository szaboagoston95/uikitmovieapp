//
//  MovieApiRequests.swift
//  UIkitMovieApp
//
//  Created by Szabó Ágoston on 2020. 04. 09.
//  Copyright © 2020. Szabo Agoston. All rights reserved.
//

import RxSwift
import RxCocoa

let apiKey = "555dd34b51d2f5b7f9fdb39e04986933"

final class MovieApiRequests {
    
    static let shared = MovieApiRequests()
    
    let imageUrl = "https://image.tmdb.org/t/p/w500/"
    let apiQueryUrl = "https://api.themoviedb.org/3/search/movie?api_key=\(apiKey)"
    let detailedMovieUrl = "https://api.themoviedb.org/3/movie/"
    
    func search(keyWord: String, completion: @escaping (_ result: [Movie]) -> ()) {
        
        var keyWord = keyWord
        keyWord = keyWord.replacingOccurrences(of: " ", with: "+")
        var tempMovieIdList = [Movie]()
        if(keyWord == "") {
            completion(tempMovieIdList)
        }
        self.searchFirstPage(keyWord: keyWord) { jsonResponse in
            guard let jsonResponse = jsonResponse else {
                return
            }
//            let pages = jsonResponse.totalPages
            guard let results = jsonResponse.results else {
                return
            }
            tempMovieIdList = results
            completion(results)
        }
    }
    
    func searchFirstPage(keyWord: String, completion: @escaping (_ response: JsonResponse?) -> ()) {
        if(keyWord != "") {
            guard let url = URL(string: "\(apiQueryUrl)&query="+keyWord) else { return }
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                
                guard let data = data, error == nil else { return }
                let responseJson = try! JSONDecoder().decode(JsonResponse.self, from: data)
                
                let pages = responseJson.totalPages
                
                print("Oldalak: ", pages)
                
                completion(responseJson)
            }
            task.resume()
        }
    }
    
//    func searchOtherPages(keyWord: String, page: Int, completion: @escaping (_ movies: [Movie]) -> ()) {
//        guard let url = URL(string: "\(apiQueryUrl)&page=\(page)&query=Star+Wars") else { return }
//        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
//            guard let data = data, error == nil else { return }
//            let pageResponse = try! JSONDecoder().decode(JsonResponse.self, from: data)
//            guard let results = pageResponse.results else { return }
//            completion(results)
//
//        }
//        task.resume()
//    }
//
    func downloadImage(movie: Movie) -> Observable<Data>? {
        if let imagePath = movie.imagePath {
            guard let url = URL(string: "\(imageUrl)\(imagePath)") else { return nil }
            let request = URLRequest(url: url)
            
            return URLSession.shared.rx.data(request: request)
        } else {
            return nil
        }
    }
    
    func searchDetailedMovie(movie: Movie, completion: @escaping (_ movies: DetailedMovie) -> ()) {
        guard let url = URL(string: "\(detailedMovieUrl)\(movie.id)?api_key=\(apiKey)") else { return }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data, error == nil else { return }
            let detailedMovie = try! JSONDecoder().decode(DetailedMovie.self, from: data)
            
            completion(detailedMovie)
        }
        task.resume()
    }
}
