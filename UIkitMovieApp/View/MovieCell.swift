//
//  MovieCell.swift
//  UIkitMovieApp
//
//  Created by Szabó Ágoston on 2020. 04. 14..
//  Copyright © 2020. Szabo Agoston. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class MovieCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var movie: Movie? {
        didSet {
            titleLabel.font = UIFont.mainFont(ofSize: 17, withWeight: .regular)
            
            if let date = movie?.releaseDate {
                if(date != "") {
                    let year = date.prefix(4)
                    self.titleLabel.text = "\(movie!.title) (\(String(describing: year)))"
                } else {
                    self.titleLabel.text = movie!.title
                }
            } else {
                self.titleLabel.text = movie!.title
            }
            
        }
    }
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
