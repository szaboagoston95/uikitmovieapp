//
//  ViewController.swift
//  UIkitMovieApp
//
//  Created by Szabó Ágoston on 2020. 04. 07..
//  Copyright © 2020. Szabo Agoston. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Toast_Swift

class RootViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search movies"
        searchController.searchBar.sizeToFit()
        searchController.searchBar.searchBarStyle = .prominent

        return searchController
    }()
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    let disposeBag = DisposeBag()
    let movieListViewModel = MovieListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        bindViews()
        setupSearch()
        
    }
    
    func setupView() {
        
        navigationItem.title = movieListViewModel.title
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.mainFont(ofSize: 17, withWeight: .bold)]
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font: UIFont.mainFont(ofSize: 34, withWeight: .bold)]
        navigationItem.searchController = searchController
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.tableFooterView = UIView()
    }
    
    func setupSearch() {
        self.searchController.searchBar.rx.text
        .orEmpty
            .debounce(DispatchTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
        .distinctUntilChanged()
        .filter { $0.count > 1 }
        .subscribe(onNext: { [unowned self] query in
            print(query)
            self.movieListViewModel.getMovies(keyWord: query)
            self.tableView.reloadData()
        }, onError: { error in
                self.view.makeToast(error.localizedDescription)
        })
        .disposed(by: disposeBag)
    }
    
    
    func bindViews() {
        
        movieListViewModel.progressViewIsHidden.observeOn(MainScheduler.instance).subscribe(activityIndicator.rx.isHidden).disposed(by: disposeBag)
        
        self.movieListViewModel
            .movies
            .observeOn(MainScheduler.instance)
            .bind(to: self.tableView.rx.items(cellIdentifier: "movieCell", cellType: MovieCell.self)) { row, movie, cell in
                cell.movie = movie
        }.disposed(by: disposeBag)
        
        
        tableView.rx.modelSelected(Movie.self)
            .subscribe(onNext: { [weak self] movie in
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle(identifier: "Main"))
                let viewController = storyboard.instantiateViewController(withIdentifier: "MovieDetailViewController") as! MovieDetailViewController
                viewController.movie = movie
                self?.navigationController?.pushViewController(viewController, animated: true)
                }, onError: { error in
                    self.view.makeToast(error.localizedDescription)
            })
            .disposed(by: disposeBag)
        
    }
}

