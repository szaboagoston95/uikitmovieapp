//
//  MovieDetailViewController.swift
//  UIkitMovieApp
//
//  Created by Szabó Ágoston on 2020. 04. 10..
//  Copyright © 2020. Szabo Agoston. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class MovieDetailViewController: UIViewController {
    
    var movieDetailViewModel = MovieDetailViewModel()
    var movie: Movie!
    
    let numberFormatter = NumberFormatter()
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var budgetLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageProgressView: UIActivityIndicatorView!
    @IBOutlet weak var detailProgressView: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        movieDetailViewModel.getDetailedMovie(movie: movie)
        setupView()
        setupBindings()
    }
    
    func setupView() {
        titleLabel.font = UIFont.mainFont(ofSize: 22, withWeight: .bold)
        ratingLabel.font = UIFont.mainFont(ofSize: 20, withWeight: .bold)
        budgetLabel.font = UIFont.mainFont(ofSize: 20, withWeight: .bold)
        descriptionLabel.font = UIFont.mainFont(ofSize: 17, withWeight: .regular)
    }
    
    func setupBindings() {
        
        // detailProgressView
        movieDetailViewModel.detailProgressViewIsHidden.observeOn(MainScheduler.instance).subscribe(detailProgressView.rx.isHidden).disposed(by: disposeBag)
        
        // imageProgressView
        movieDetailViewModel.imageProgressViewIsHidden.observeOn(MainScheduler.instance).subscribe(imageProgressView.rx.isHidden).disposed(by: disposeBag)
        
        // Image
        movieDetailViewModel.getImage(movie: movie)?.map { data in
            UIImage(data: data)
        }
        .observeOn(MainScheduler.instance)
        .catchErrorJustReturn(nil)
        .subscribe(imageView.rx.image)
        .disposed(by: disposeBag)
        
        // Title
        if let date = movie?.releaseDate {
            if(date != "") {
                let year = date.prefix(4)
                self.titleLabel.text = "\(movie!.title) (\(String(describing: year)))"
            } else {
                self.titleLabel.text = movie!.title
            }
        } else {
            self.titleLabel.text = movie!.title
        }
        
        // Rating
        movieDetailViewModel.detailedMovie
            .map { detailedMovie in
                if let rating = detailedMovie.rating {
                    return "Rating: " + String(format: "%.01f", rating) + "/10"
                }
                return "Rating: -"
        }
        .observeOn(MainScheduler.instance)
        .subscribe(ratingLabel.rx.text)
        .disposed(by: disposeBag)
        
        // Budget
        movieDetailViewModel.detailedMovie
            .map { detailedMovie in
                if let budget = detailedMovie.budget {
                    self.numberFormatter.numberStyle = NumberFormatter.Style.decimal
                    self.numberFormatter.groupingSeparator = " "
                    let formattedBudget = self.numberFormatter.string(from: NSNumber(value:budget))
                    return budget != 0 ? "Budget: \(formattedBudget!)$" : "Budget: -"
                }
                return "Budget: -"
        }
        .observeOn(MainScheduler.instance)
        .subscribe(budgetLabel.rx.text)
        .disposed(by: disposeBag)
        
        // Description
        movieDetailViewModel.detailedMovie
            .map { detailedMovie in
                return detailedMovie.overView != "" ? detailedMovie.overView : "No description found"
        }
        .observeOn(MainScheduler.instance)
        .subscribe(descriptionLabel.rx.text)
        .disposed(by: disposeBag)
    }
    
}
