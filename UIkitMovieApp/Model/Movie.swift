//
//  Movie.swift
//  UIkitMovieApp
//
//  Created by Szabó Ágoston on 2020. 04. 09..
//  Copyright © 2020. Szabo Agoston. All rights reserved.
//

import Foundation
import UIKit

struct Movie: Decodable {
    var id: Int
    var title: String
    var releaseDate: String?
    var imagePath: String?
    var rating: Double?
    var overView: String
    var budget: Int?
    
    private enum CodingKeys: String, CodingKey {
        case imagePath = "poster_path"
        case id
        case title
        case rating = "vote_average"
        case overView = "overview"
        case releaseDate = "release_date"
        
    }
}

struct JsonResponse: Decodable {
    var results: [Movie]?
    var totalPages: Int
    var totalResults: Int
    
    private enum CodingKeys: String, CodingKey {
        case results
        case totalResults = "total_results"
        case totalPages = "total_pages"
    }
}

struct DetailedMovie: Decodable {
    var id: Int
    var title: String
    var releaseDate: String?
    var imagePath: String?
    var rating: Double?
    var overView: String
    var budget: Int?
    
    private enum CodingKeys: String, CodingKey {
        case imagePath = "poster_path"
        case id
        case title
        case rating = "vote_average"
        case overView = "overview"
        case releaseDate = "release_date"
        case budget
    }
}


