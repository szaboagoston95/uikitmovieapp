//
//  MovieListViewModel.swift
//  UIkitMovieApp
//
//  Created by Szabó Ágoston on 2020. 04. 09..
//  Copyright © 2020. Szabo Agoston. All rights reserved.
//

import RxSwift

class MovieListViewModel {
    weak var service: MovieApiRequests?
    
    let title = "Movies"
    
    var movies: BehaviorSubject<[Movie]> = BehaviorSubject(value: [])
    var progressViewIsHidden: BehaviorSubject<Bool> = BehaviorSubject(value: true)
    
    init(service: MovieApiRequests = MovieApiRequests.shared) {
        self.service = service
    }
    
    func getMovies(keyWord: String) {
        self.progressViewIsHidden.onNext(false)
        service!.search(keyWord: keyWord) { results in
            self.movies.onNext(results)
            self.progressViewIsHidden.onNext(true)
        }
    }
}
