//
//  DetailedMovieViewModel.swift
//  UIkitMovieApp
//
//  Created by Szabó Ágoston on 2020. 04. 17..
//  Copyright © 2020. Szabo Agoston. All rights reserved.
//

import RxSwift

class MovieDetailViewModel {
    weak var service: MovieApiRequests?
    
    var detailedMovie: BehaviorSubject<DetailedMovie> = BehaviorSubject(value: DetailedMovie.init(id: 0, title: "", releaseDate: "", imagePath: "", rating: 0.0, overView: "", budget: 0))
    var imageProgressViewIsHidden: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    var detailProgressViewIsHidden: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    
    init(service: MovieApiRequests = MovieApiRequests.shared) {
        self.service = service
    }
    
    func getImage(movie: Movie) -> Observable<Data>? {
        self.imageProgressViewIsHidden.onNext(false)
        guard let data = service?.downloadImage(movie: movie) else {
            self.imageProgressViewIsHidden.onNext(true)
            return nil
        }
        self.imageProgressViewIsHidden.onNext(true)
        return data
    }
    
    func getDetailedMovie(movie: Movie) {
        self.detailProgressViewIsHidden.onNext(false)
        service?.searchDetailedMovie(movie: movie) { detailedMovie in
            self.detailedMovie.onNext(detailedMovie)
            self.detailProgressViewIsHidden.onNext(true)
        }
    }
}
